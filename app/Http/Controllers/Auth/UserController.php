<?php

    namespace App\Http\Controllers\Auth;
    use Auth;
    use App\Models\User;
    use App\Models\Address;
    use App\Models\Contact;
    use Validator;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\DB;
    use App\Http\Requests\LoginRequest;
    use App\Http\Resources\User\UserResource;

class UserController extends Controller
{
    //public function __construct() {
      //  $this->middleware(['auth:api']);
   // }
        public function index()
        {
            return response()->json([
                Address::with(['users'])->get(),
                Contact::with(['users'])->get()
            ]);
        }
        public function login(LoginRequest $request)
        {
            $request = Request::create('/oauth/token', 'POST', [
                'grant_type' => 'password',
                'client_id' => config('services.vue_client.id'),
                'client_secret' => config('services.vue_client.secret'),
                'username' => $request->username,
                'password' => $request->password,
            ]);
            
            return app()->handle($request);
        }
        public function logout()
        {
            $accessToken = auth()->user()->token();
    
            DB::table('oauth_access_tokens')->where('id', $accessToken->id)->delete();
            DB::table('oauth_refresh_tokens')->where('access_token_id', $accessToken->id)->delete();
    
            return response()->json(['status' => 200]);
        }
        public function details(Request $request) {
            return new UserResource($request->user());
        }


}
