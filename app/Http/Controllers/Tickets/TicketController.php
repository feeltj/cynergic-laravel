<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class TicketController extends Controller
{
    public function index()
    {
        $tickets = DB::table('tickets')
                   ->join('user_info', 'tickets.user_id', 'user_info.id')
                   ->where('user_info.id', Auth::user()->id)
                   ->select('tickets.id', 'tickets.ticketStatus', 'tickets.summary', 'tickets.contactName', 'tickets.priority', 'tickets.created_at', 'tickets.priority')
                   ->get();

        return response()->json([
            'tickets' => $tickets
        ]);
    }
    public function create(){
        $departments = DB::table('departments')->get();

        return response()->json([
            'departments' => $departments
        ]);
    }

    public function store(Request $request) {
        $tickets_add = DB::table('tickets')->insertGetId(
            [
                'contactName' => $request->input('contactName'),
                'email' => $request->input('email'),
                'user_id' => Auth::user()->id,
                'department_id' => $request->input('department_id'),
                'summary' => $request->input('summary'),
                'detailDescription' => $request->input('detailDescription'),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]
        );
        return response()->json([
           $tickets_add
        ]);
    }
}
