<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Rate;
use App\Models\Post;
use App\Models\Contact;
use App\Models\Defaults;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Contracts\AddressContract;
use App\Contracts\ContactContract;
use App\Contracts\UserinfoContract;
use App\Contracts\UserrateContract;
use App\Contracts\UserRateProvidersContract;
use App\Contracts\UserRateDefaultsContract;
use App\Dashboard;
use Auth;
use DB;

class DashboardController extends BaseController
{
    protected $addressRepository;
    protected $contactRepository;
    protected $userinfoRepository;
    protected $userrateRepository;
    protected $userRateProvidersRepository;
    protected $userRateDefaultsRepository;

    public function __construct(AddressContract $addressRepository,
                               ContactContract $contactRepository, 
                               UserinfoContract $userinfoRepository, 
                               UserrateContract $userrateRepository,
                               UserRateProvidersContract $userRateProvidersRepository,
                               UserRateDefaultsContract $userRateDefaultsRepository
                               ) {
       $this->addressRepository = $addressRepository;
       $this->contactRepository = $contactRepository;
       $this->userinfoRepository = $userinfoRepository;
       $this->userrateRepository = $userrateRepository;
       $this->userRateProvidersRepository = $userRateProvidersRepository;
       $this->userRateDefaultsRepository = $userRateDefaultsRepository;
    }
    public function index(Dashboard $serviceCount)
    {
        //dd($service->getServiceSummary());
        $address = $this->addressRepository->listAddress();
        $contact = $this->contactRepository->listContact();
        $user_info = $this->userinfoRepository->listUserinfo();
        $u_rate = $this->userrateRepository->listUserrate();
        $u_providers = $this->userRateProvidersRepository->listUserRateProviders();
        $u_defaults = $this->userRateDefaultsRepository->listUserRateDefaults();

        $user_rate = Rate::with('users')->get();
        $address = Post::with('users')->get();
        $contacts = Contact::with('users')->get();
        $defaults = Defaults::with('rates')->get();
        $user_provider = Rate::with('provider')->get();
        
        return response()->json([
            'users' => $serviceCount->getServiceSummary(),
        ]);
    }
    public function getActiveServices(Dashboard $service)
    {
        $u = Auth::user()->id;

        $user_info =  DB::table('user_rate_infos')
                ->join('user_rates', 'user_rate_infos.rate_id', '=', 'user_rates.id')
                ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
                ->join('user_rates_defaults', 'user_rates.default_id', '=', 'user_rates_defaults.id')
                ->select('user_info.id','user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id','user_rates_defaults.class_id')
                
                ->where('status', 'active')
                ->where('user_info.id', $u)
                ->groupBy('user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id', 'user_rates_defaults.class_id','user_info.id')
                ->get();
             
    foreach($user_info as $uinfo){

                $table = $service->getUserRateUsageTable($uinfo->id);
                $usage = $service->getUsage($uinfo->id,$table,'','',true);
               
                return response()->json([
                   //'userInfo'=> $user_info,
                   'services' => $usage 
                ]);
    }
}

public function getUserData()
{
    $u = Auth::user()->id;
    $user_data = DB::table('user_info')
                     ->select('user_info.id','user_info.username','user_info.reciptto',
                             'contacts.company', 'contacts.work', 'contacts.firstname',
                             'contacts.lastname', 'contacts.acn','contacts.mobile', 
                             'contacts.home','contacts.fax','address.street',
                             'address.city','address.postcode', 'address.state')
                     ->join('contacts', 'contacts.id', 'user_info.contact_id')
                     ->join('address', 'address.id', 'user_info.post_id')
                     ->where('user_info.id', '=', $u)
                     ->get()
                     ;
        return response()->json([
            'userdata' => $user_data
        ]);
}
public function update(Request $request)
{
    $user_rates = DB::table('user_rate_infos')
    ->join('user_rates', 'user_rate_infos.rate_id', '=', 'user_rates.id')
    ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
    ->where('user_info.id', '=', Auth::user()->id)

    ->select('user_info.id','user_info.username','user_rates.description', 'user_info.rate_id')
    ->update([
        //'user_rates.id' =>$request->input('rate_id'),
        'description' => $request->input('description')
    ]);
  return response()->json([
    $user_rates
  ]);
}
public function updateUserData(Request $request)
{
    $u = Auth::user()->id;
    $user_data = DB::table('user_info')
    ->select('user_info.id','user_info.username','user_info.reciptto',
    'contacts.company', 'contacts.work', 'contacts.firstname',
    'contacts.lastname', 'contacts.acn','contacts.mobile', 
    'contacts.home','contacts.fax','address.street',
    'address.city','address.postcode', 'address.state')
    ->join('contacts', 'contacts.id', 'user_info.contact_id')
    ->join('address', 'address.id', 'user_info.post_id')
    ->where('user_info.id', '=', $u)
    ->update([
        //'user_info.username'=>$request->input('username'),
        'user_info.reciptto' => $request->input('reciptto'),
        'contacts.company' => $request->input('company'),
        'contacts.acn' => $request->input('acn'),
        'contacts.firstname' => $request->input('firstname'),
        'contacts.lastname' => $request->input('lastname'),
        'contacts.work' => $request->input('work'),
        'contacts.mobile' => $request->input('mobile'),
        'contacts.home' => $request->input('home'),
        'contacts.fax' => $request->input('fax'),
        'address.street' => $request->input('street'),
        'address.city' => $request->input('city'),
        'address.postcode' => $request->input('postcode'),
        'address.state' => $request->input('state')
    ]); 
    return response()->json([
        $user_data
    ]);
}
}
