<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Dashboard;
use App\Services;
use Auth;
use DB;

class ServicesController extends BaseController
{
    public function headService(Dashboard $serviceCount)
    {    
        return response()->json([
            'header' => $serviceCount->getServiceSummary(),
        ]);
    }
    public function index(Dashboard $service)
    {
        $u = Auth::user()->id;

        $user_info =  DB::table('user_rate_infos')
                ->join('user_rates', 'user_rate_infos.rate_id', '=', 'user_rates.id')
                ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
                ->join('user_rates_defaults', 'user_rates.default_id', '=', 'user_rates_defaults.id')
                ->select('user_info.id','user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id','user_rates_defaults.class_id')
                
                ->where('status', 'active')
                ->where('user_info.id', $u)
                ->groupBy('user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id', 'user_rates_defaults.class_id','user_info.id')
                ->get();
             
    foreach($user_info as $uinfo){

                $table = $service->getUserRateUsageTable($uinfo->id);
                //dd($table);
                $usage = $service->getUsage($uinfo->id,$table,'','',true);
               
                return response()->json([
                   //'userInfo'=> $user_info,
                   'services' => $usage 
                ]);
    }
    }

    public function getHosting()
    {
        $hostings = DB::table('virtual_servers')
                      ->join('user_info', 'user_info.username', 'virtual_servers.domain_user')
                      ->join('user_rate_infos', 'user_rate_infos.user_id', 'virtual_servers.host_billing_userid')
                      ->join('user_rates', 'user_rate_infos.rate_id', 'user_rates.id')
                      ->join('user_rates_defaults', 'user_rates.default_id','user_rates_defaults.id')
                      ->join('user_web_traffic_daily', 'user_info.id', 'user_web_traffic_daily.server_ip')
                      ->select('user_info.id','user_rate_infos.rate_id','virtual_servers.domain_user', 'virtual_servers.domain_name', 'user_rates.description', 'user_web_traffic_daily.bytes')
                      ->where('user_info.status', '=' ,'active')
                      ->where('user_rates_defaults.class_id', '=', 'hosting')
                      ->where('virtual_servers.host_billing_userid', Auth::user()->id)
                      ->get();
        return response()->json([
            'hostings' => $hostings
        ]);
    }

    public function getDomains(Services $services)
    {
        //dd($services->getServices_hosting());
        $domains = DB::table('virtual_servers')
            ->join('user_info', 'user_info.username', 'virtual_servers.domain_user')
            ->join('user_rate_infos', 'user_rate_infos.user_id', 'virtual_servers.domain_billing_userid')
            ->join('user_rates', 'user_rate_infos.rate_id', 'user_rates.id')
            ->join('user_rates_defaults', 'user_rates.default_id','user_rates_defaults.id')
            ->join('virtual_servers_data', 'virtual_servers.id','virtual_servers_data.domain_id')
            ->select('user_info.id','user_rate_infos.rate_id','virtual_servers.domain_user',
                       'virtual_servers.dns_master_ip', 'virtual_servers.domain_name', 
                       'virtual_servers.expiry','user_rates_defaults.description',
                       'virtual_servers_data.name', 'virtual_servers_data.value',
                        'user_info.expiry_date'
                       )
            ->where('user_info.status', '=' ,'active')
            ->where('virtual_servers.deleted', '=', 'no')
            ->where('domain_billing_userid', '>', 0)
            ->where('user_rates_defaults.class_id', '=', 'domains')
            ->where('virtual_servers.domain_billing_userid', Auth::user()->id)
            ->orderBy('virtual_servers.domain_name')
        ->get();
        return response()->json([
            'domains' => $domains
        ]);
    }

    public function updateDomain(Request $request)
    {
        $virtual_domains = DB::table('virtual_servers_data')
                          ->join('virtual_servers', 'virtual_servers.id', 'virtual_servers_data.domain_id')
                          ->join('user_info', 'user_info.id', 'virtual_servers.domain_billing_userid')
                          ->where('user_info.id', '=', Auth::user()->id)
                          ->select('virtual_servers_data.domain_id','virtual_servers_data.name', 'virtual_servers_data.value')
                          ->update([
                               'name' => $request->input('name'),
                               'value' => $request->input('value')
                          ]);                          ;
                          return response()->json([
                            $virtual_domains
                          ]);
    }

    public function getEmails()
    {
        $emails = DB::table('email')
                ->join('user_info', 'user_info.id', 'email.ownerid')
                ->join('email_vacations', 'user_info.id', 'email_vacations.user_id')
                ->join('project_git_user_maildisk', 'user_info.username', 'project_git_user_maildisk.username')
                ->join('email_vacations_addresses', 'email.address', 'email_vacations_addresses.address')
                ->where('email_vacations.status', 'active')
                ->where('email_vacations_addresses.user_id', Auth::user()->id)
                ->select('user_info.username', 'project_git_user_maildisk.disk_usage', 'project_git_user_maildisk.last_login', 'email.address')
                ->get();
           return response()->json([
             'emails' => $emails
           ]);
    }
}
