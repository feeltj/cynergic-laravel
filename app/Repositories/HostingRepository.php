<?php

namespace App\Repositories;

use App\Models\Hosting;
use App\Contracts\HostingContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;


class HostingRepository extends BaseRepository implements HostingContract {

    public function __construct(Hosting $model) {
        parent::__construct($model);
        $this->model = $model;
    }
    public function listHostings(string $order = 'id', $sort = 'desc', array $columns  = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    public function getHostingById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }
    public function updateHosting(array $params)
    {
        $hosting = $this->getHostingById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('domain_name', 'domain_file', 'domain_user', 'web_ip', 'database_name', 'database_username', 'database_password'));

        $hosting->update($merge->all());

        return $hosting;

    }
    public function deleteHosting($id)
    {
       $hosting = $this->getHostingById($id);

       $hosting->delete();

       return $hosting;
    }
}