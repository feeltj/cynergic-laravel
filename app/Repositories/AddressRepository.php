<?php

namespace App\Repositories;

use App\Models\Post;
use App\Contracts\AddressContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;


class AddressRepository extends BaseRepository implements AddressContract {

    public function __construct(Post $model) {
        parent::__construct($model);
        $this->model = $model;
    }
    public function listAddress(string $order = 'id', $sort = 'desc', array $columns  = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    public function findAddressById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }
    public function updateAddress(array $params)
    {
        $address = $this->findAddressById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('street', 'city', 'postcode', 'country', 'state'));

        $address->update($merge->all());

        return $address;

    }
    public function deleteAddress($id)
    {
       $address = $this->findAddressById($id);

       $address->delete();

       return $address;
    }
}