<?php

namespace App\Repositories;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Models\Provider;

use App\Contracts\UserRateProvidersContract;

class UserRateProvidersRepository extends BaseRepository implements UserRateProvidersContract
{
     public function __construct(Provider $model)
     {
         parent::__construct($model);
         $this->model = $model;
     }

     public function listUserRateProviders(string $order = 'id', string $sort = 'order', array $columns = ['*'])
     {
         return $this->all($columns, $order, $sort);
     }

     public function findUserRateProviderById(int $id)
     {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
     }
     public function updateUserRateProvider(array $params)
     {
         $user_rate_providers = $this->findUserRateProviderById($params['id']);
         $collection = collect($params)->except('_token');
         $merge = $collection->merge(compact('admin_name', 'cust_name','sku','status'));
         $user_rate_providers->update($merge->all());

         return $user_rate_providers;
     }
}