<?php

namespace App\Repositories;

use App\Models\Domain;
use App\Contracts\DomainContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;


class DomainRepository extends BaseRepository implements DomainContract {

    public function __construct(Domain $model) {
        parent::__construct($model);
        $this->model = $model;
    }
    public function listDomains(string $order = 'id', $sort = 'desc', array $columns  = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    public function findDomainById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }
    public function updateDomain(array $params)
    {
        $domain = $this->findDomainById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('server_ip', 'domain_name', 'date', 'bytes', 'status'));

        $domain->update($merge->all());

        return $domain;

    }
    public function deleteDomain($id)
    {
       $domain = $this->findDomainById($id);

       $domain->delete();

       return $domain;
    }
}