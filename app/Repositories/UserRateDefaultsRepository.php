<?php

namespace App\Repositories;

use App\Contracts\UserRateDefaultsContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Models\Defaults;

class UserRateDefaultsRepository extends BaseRepository implements UserRateDefaultsContract
{
    public function __construct(Defaults $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function listUserRateDefaults(string $order ='id', string $sort = 'order', array $columns =['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    public function findUserRateDefaultsById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }

    public function updateUserRateDefaults(array $params)
    {
        $user_rate_defaults = $this->findUserRateDefaultsById($params['id']);
        $collection = collect($params)->except('_token');
        $merge = $collection->merge(compact('dealer_id', 'description'));

        $user_rate_defaults->update($merge->all());

        return $user_rate_defaults;
    }
}