<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Contracts\ContactContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class ContactRepository extends BaseRepository implements ContactContract {

    public function __construct(Contact $model) {
        parent::__construct($model);
        $this->model = $model;
    }

    public function listContact(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    public function findContactById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }
    public function updateContact(array $params)
    {
       $contact = $this->findContactById($params['id']);
       $collection = collect($params)->except('_token');

       $merge = $collection->merge(compact('firstname', 'lastname', 'company', 'acn', 'home', 'work', 'fax','mobile'));

       $contact->update($merge->all());

       return $contact;
    }

    public function deleteContact($id)
    {
        $contact = $this->findContactById($id);
        $contact->delete();
        return $contact;
    }
}