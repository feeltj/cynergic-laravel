<?php
namespace App\Repositories;

use App\Contracts\UserrateContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Models\Rate;

class UserrateRepository extends BaseRepository implements UserrateContract
{
    public function __construct(Rate $model){
        parent::__construct($model);
        $this->model = $model;
    }
    public function listUserrate(string $order = 'id', string $sort = 'order', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    public function findUserRateById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }
    public function updateUserRate(array $params)
    {
        $user_rate = $this->findUserRateById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('description','rate_type', 'plan_type'));
        $user_rate->update($merge->all());

        return $user_rate;
    }
}