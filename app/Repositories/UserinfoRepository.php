<?php
namespace App\Repositories;

use App\Contracts\UserinfoContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use App\Models\User;

class UserinfoRepository extends BaseRepository implements UserinfoContract {
    public function __construct(User $model) {
        parent::__construct($model);
        $this->model = $model;
    }
    public function listUserinfo(string $order = 'id', string $sort ='order', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }

    public function findUserinfoById(int $id){
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    } 

    public function updateUserinfo(array $params) {
        $userinfo = $this->findUserinfoById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('username', 'email', 'recipt', 'status', 'balance'));
        $userinfo->update($merge->all());

        return $userinfo;
    }

    public function deleteUserinfo($id){
        $userinfo = $this->findUserinfoById($id);
        $userinfo->delete();
        return $userinfo;
    }
}

