<?php

namespace App\Repositories;

use App\Models\Email;
use App\Contracts\EmailContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;


class EmailRepository extends BaseRepository implements EmailContract {

    public function __construct(Email $model) {
        parent::__construct($model);
        $this->model = $model;
    }
    public function listEmails(string $order = 'id', $sort = 'desc', array $columns  = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    public function findEmailById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }
    public function updateEmail(array $params)
    {
        $email = $this->findEmailById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('owner_id', 'address', 'destination', 'modified', 'creation'));

        $email->update($merge->all());

        return $email;

    }
    public function deleteEmail($id)
    {
       $email = $this->findEmailById($id);

       $email->delete();

       return $email;
    }
}