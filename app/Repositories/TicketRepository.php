<?php

namespace App\Repositories;

use App\Models\Ticket;
use App\Contracts\TicketContract;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class TicketRepository implements TicketContract 
{
    public function __construct(Ticket $model) {
        parent::__construct($model);
        $this->model = $model;
    }

    public function listTickets(string $order = 'id', $sort = 'desc', array $colums = ['*'])
    {
        return $this->all($colums, $order, $sort);
    }

    public function getTicketById($id)
    {
        try {
            return $this->findOneOrFail($id);
    
        } catch (ModelNotFoundException $e) {
    
            throw new ModelNotFoundException($e);
        }
    }

    public function updateTcket(array $params)
    {
        $ticket = $this->getTicketById($params['id']);
        $collection = collect($params)->except('_token');

        $merge = $collection->merge(compact('email', 'ticketStatus', 'summary', 'priority', 'detailDescription', 'user_id', 'department_id'));

        $ticket->update($merge->all());

        return $ticket;
    }

    public function deleteTicket($id)
    {
        $ticket = $this->getTicketById($id);

        $ticket->delete();

        return $ticket;
    }
}
