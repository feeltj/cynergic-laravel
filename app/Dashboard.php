<?php
namespace App;
use Auth;
use DB;
use App\Models\User;
use App\Contracts\UserinfoContract;
use App\Contracts\UserRateProvidersContract;
use App\Contracts\UserrateContract;
use App\Contracts\UserRateDefaultsContract;

class Dashboard {
    public $users;
    protected $userinfoRepository;
    protected $userRateProvidersRepository;
    protected $userrateRepository;
    protected $userRateDefaultsRepository;

    public function __construct(User $users,
        UserinfoContract $userinfoRepository, 
        UserRateProvidersContract $userRateProvidersRepository,
        UserrateContract $userrateRepository,
        UserRateDefaultsContract $userRateDefaultsRepository
        )
    {
        $this->userinfoRepository = $userinfoRepository;
        $this->users = $users;   
        $this->userRateProvidersRepository = $userRateProvidersRepository;
        $this->userrateRepository = $userrateRepository;
        $this->userRateDefaultsRepository = $userRateDefaultsRepository;
    }
    
    public function getServiceSummary()
    {

        $user_id = $this->getUserServiceList();
        $users = DB::table('user_rate_infos')
                ->join('user_rates', 'user_rate_infos.rate_id', '=', 'user_rates.id')
                ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
                ->join('user_rates_defaults', 'user_rates.default_id', '=', 'user_rates_defaults.id')
                ->select('user_rate_infos.rate_id','user_rates_defaults.description', 'user_rates.default_id',  (DB::raw('count(*) as user_count')))
                ->where('user_info.id', $user_id)
                ->groupBy('user_rate_infos.rate_id','user_rates_defaults.description', 'user_rates.default_id')
                ->get();
                return $users;
    }
    public function getMultipleService()
    {
        $user_id = $this->getUserServiceList();
        $users = DB::table('user_rate_infos')
                ->join('user_rates', 'user_rate_infos.rate_id', '=', 'user_rates.id')
                ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
                ->join('user_rates_defaults', 'user_rates.default_id', '=', 'user_rates_defaults.id')
                ->select('user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id',  (DB::raw('count(*) as user_count')))
                ->where('user_info.id', $user_id)
                ->groupBy('user_rate_infos.rate_id','user_rates_defaults.description', 'user_rates.default_id')
                ->get();
                dd($users);
    }

    public function getUserServiceList() {
 
		$user_id=Auth::user()->id;
		$accountIds=array($user_id);
	
        $arr = DB::table('user_info')
                   ->where('status', 'active')
                   ->where('parent_id', $user_id)
                   ->orWhere('id', $user_id)
                   ->get();
    
        $accountIds=array();
        
		foreach($arr as $user) {
            $accountIds[]=$user->id;
        }
        
		return $accountIds;
	}
    
    public function getUserRateUsageTable($uid,$returnInfo="") {

		

		$tables['hosting']=		"user_web_traffic_daily";
        $tables['data']=		"logouts";
        $tables['misc']=		"user_rates";
		//$tables['wireless']=		"logouts";
		$tables['voice_ciptel']=	"cdr_ciptelnsw";
//		$tables['voice']=		"cdr_asterisk";
		$tables['voice']=		"cdr_voip";

        $tables['voice_ptel']=		"cdr_powertel_ctop";
        
        $us_info = DB::table('user_rate_infos')
                ->join('user_rates', 'user_rate_infos.rate_id', '=', 'user_rates.id')
                ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
                ->join('user_rates_defaults', 'user_rates.default_id', '=', 'user_rates_defaults.id')
                ->select('user_info.id','user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id','user_rates_defaults.class_id')
                ->where('status', 'active')
                ->where('user_info.id', Auth::user()->id)
                ->groupBy('user_rate_infos.rate_id','user_rates.description', 'user_rates.default_id', 'user_rates_defaults.class_id','user_info.id')
                ->get();
             
                                            
    foreach($us_info as $urd){
                                                 

        $class_id=$urd->class_id;
   
		if($urd->id == 5) {
			$class_id="hosting";
		} //hack to ensure we're counting host data when required.

		if($returnInfo <> "") {
			if($returnInfo=="class_id") {
				return $class_id;
			}
			if($returnInfo == "description") {
				return $urd->description;
			}
        }
  

		if($class_id=="voice") {
			//check services to see which table this is
			//if service_code is ciptel then use ciptel, otherwise use asterisk
            $userv = DB::table('user_services')
                     ->where('isp_userid', $uid)
                     ->get();
                     foreach($userv as $user_serv){}
                    // dd($userv);
			$sc=strtolower($user_serv->service_code);
			if($sc=="ciptel") {
               
                $class_id="voice_ciptel";
                //dd($class_id);
			}
			if($sc=="ptel") {
			    $class_id="voice_ptel";
			}
        }
        //dd($class_id);



        $table=$tables[$class_id];
        //dd($table);

		if($this->_adminIP()==true) {
			echo "table:".$table."<br>";
		}

        return $table;
    }
}

    public function getUsage($uid,$table,$start="",$finish="",$total=false) {

		if($start == "") {
			$start=date("Y-m-01");
		}
		if($finish == "") {
			$finish=date("Y-m-d");
		}

		$data=array();

		switch($table) {
			case "logouts":
				$data=$this->getUsage_logouts($uid,$start,$finish,$total);
                break;
            case "user_rates":
				$data=$this->getUsage_user_rates();
				break;
			case "user_web_traffic_daily":
				$data=$this->getUsage_user_web_traffic_daily($uid,$start,$finish,$total);
				break;
			case "cdr_ciptelnsw":
				$data=$this->getUsage_cdr_ciptel($uid,$start,$finish,$total);
                break;
                
			case "cdr_asterisk":
				$data=$this->getUsage_cdr_ciptel($uid,$start,$finish,$total,"cdr_asterisk");
				break;
			case "cdr_powertel_ctop":
			        $data=$this->getUsage_cdr_powertel_ctop($uid,$start,$finish,$total);
				break;
		}

		return $data;

    }
    public function getUsage_user_rates() 
    {
        
      $user_rates = DB::table('user_rates')
            ->join('user_rate_infos', 'user_rates.id', '=', 'user_rate_infos.rate_id')
            ->join('user_info', 'user_rate_infos.user_id', '=', 'user_info.id')
            ->where('user_info.id', '=', Auth::user()->id)
            ->select('user_info.id','user_info.username','user_rates.description', 'user_info.rate_id')
            ->get();
    
       return $user_rates;
    }
    public function getUserServices_FnnByUserId($userId) {

        //$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        
	
        
        $serv = DB::table('user_services')
                    ->join('user_info', 'user_services.isp_userid', '=', 'user_info.id')
                    ->select(DB::raw('user_services.fnn'))

                    ->get()
                    ;
                    //dd($serv);
                    foreach($serv as $s){
                        //dd($s->fnn);
                    }
		return $s->fnn;

	}
    
    public function getUsage_logouts($uid,$start,$finish,$total=false) {


		if($start=="") {
			$start=date("Y-m-01 00:00:00");
		}
		if($finish=="") {
			$finish=date("Y-m-d H:i:s");
		}

		if($total==true) {
            $query = DB::table('logouts')
                      ->select('logouts.uid','user_info.username','user_rate_infos.rate_id','user_rates.description', 'logouts.creation',DB::raw('SUM(intraffic) as down, SUM(outtraffic) as up'))
                      ->join('user_info', 'logouts.uid', '=', 'user_info.id')
                      ->join('user_rate_infos', 'user_rate_infos.rate_id', '=', 'logouts.rate_id')
                      ->join('user_rates', 'logouts.rate_id', '=', 'user_rates.id')
                      ->groupBy('uid','creation', 'user_info.username','user_rates.description', 'user_rate_infos.rate_id')
                      ->where('uid','=', $uid)
                      ->where('indate', '>=', $start)
                      ->orWhere('indate',  '<=', $finish)
                      ->get();
                    //dd($query);
                    
		} else {

            $query = DB::table('logouts')
                    ->select('creation',  DB::raw('SUM(intraffic) as down', 'SUM(outtraffic) as up'))
                    ->groupBy('creation')
                    ->where('uid', $uid)
                    ->where('indate',  '<=', $finish)
                    ->orderByRaw('creation ASC')
                    ->get();
        }
    

		return $query;
      
    }

    public function getUsage_user_web_traffic_daily($domain,$start,$finish,$total=true) {

	
		if($total==true) {
	    
            $query = DB::table('user_web_traffic_daily')
                      ->join('user_info', 'user_info.id', '=','user_web_traffic_daily.server_ip')
                      ->join('virtual_servers',  'virtual_servers.host_billing_userid', '=','user_web_traffic_daily.server_ip')
                      ->select(DB::raw('date as creation,virtual_servers.domain_name,SUM(bytes) as up'))
                      ->groupBy('bytes','date', 'virtual_servers.domain_name')
                      //->where('user_web_traffic_daily.domain_name', $domain)
                      ->where('user_info.id', Auth::user()->id)
                      ->where('date', '>=', $start)
                      ->where('date','<=',$finish)
                      ->get();
                      //dd($query);

		} else {
		
            $query = DB::table('user_web_traffic_daily')
            ->select(DB::raw('date as creation, domain_name,bytes as up'))
            ->where('domain_name', $domain)
            ->where('date', '>=', $start)
            ->where('date','<=',$finish)
            ->get();
		}


		$out=array();

		foreach($query as $row) {
            $date=$row->creation;
            $domain_name = $row->domain_name;
			$date=date("jS",strtotime($date));
			$up=ceil(($row->up/1000000));
            $node=array("date"=>$date,"up"=>$up, "domain_name" => $domain_name);
			$out[]=$node;
		}

		return $out;

    }
    
    public function getUsage_cdr_ciptel($uid,$start,$finish,$total=true,$table="cdr_ciptelnsw") {


        $fnn=$this->getUserServices_FnnByUserId($uid);

		if($total==false) {
            $query = DB::table($table)
                         ->select(DB::raw('calldate as creation,SUM(billsec) as down, COUNT(calldate) as up'))
                         ->groupBy('calldate')
                         ->where('disposition', '=', 'ANSWERED')
                         ->where('accountcode', '=', $fnn)
                         ->orWhere('calldate','>=', $start)
                         ->orWhere('calldate', '<=', $finish)
                         ->where('price_rated', '>',  '-0.0001')
                         ->orderBy('calldate')->get();
                         //dd($query);


		} else {
		
            $query = DB::table($table)
                ->select(DB::raw('calldate as creation,SUM(billsec) as down, COUNT(calldate) as up'))
                ->groupBy('calldate')
                ->where('disposition', '=', 'ANSWERED')
                ->where('accountcode', '=', $fnn)
                ->orWhere('calldate','>=', $start)
                ->orWhere('calldate', '<=', $finish)
                ->where('price_rated', '>',  '-0.0001')
                ->orderBy('calldate')->get();
                //dd($query);
        
		}
		
		foreach($query as $row) {
			$date=$row->creation;
            $date=date("jS",strtotime($date));
            $up=$row->up;
            $node=array("date"=>$date,"up"=>$up,"down"=>$row->down,"calls"=>$row->up,"calltime"=>$row);
			$out[]=$node;
		}

		return $out;

	}
    
    public function _adminIP() {

	    $adminIps=array("203.31.101.237","103.29.77.251","203.57.78.150","58.96.114.154");
	    if(in_array($_SERVER['REMOTE_ADDR'],$adminIps)) {
		return true;
	    } else {
		return false;
	    }

	}
}