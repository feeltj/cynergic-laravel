<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRateInfo extends Model
{
    protected $table = 'user_rate_infos';
    protected $timestamps = false;
}
