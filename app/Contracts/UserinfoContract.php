<?php

namespace App\Contracts;

interface UserinfoContract {
    public function listUserinfo(string $order = 'id', string $sort ='order', array $columns = ['*']);
    public function findUserinfoById(int $id);
    public function updateUserinfo(array $params);
    public function deleteUserinfo($id);
}