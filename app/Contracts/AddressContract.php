<?php

namespace App\Contracts;

interface AddressContract 
{
     /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listAddress(string $order = 'id', $sort = 'desc', array $columns  = ['*']);
      /**
     * @param int $id
     * @return mixed
     */
   public function findAddressById(int $id);
    /**
     * @param array $params
     * @return mixed
     */


     /**
     * @param array $params
     * @return mixed
     */

    public function updateAddress(array $params);

     /**
     * @param $id
     * @return bool
     */
    public function deleteAddress($id);
}