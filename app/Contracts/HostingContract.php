<?php

namespace App\Contracts;

interface HostingContract 
{
   public function listHostings(string $order ='id', $sort = 'desc', array $columns = ['*']);

   public function getHostingById(int $id);

   public function updateHosting(array $params);

   public function deleteHosting($id);
}