<?php

namespace App\Contracts;

interface UserRateProvidersContract {
    public function listUserRateProviders(string $order = 'id', string $sort = 'order', array $columns = ['*']);
    public function findUserRateProviderById(int $id);
    public function updateUserRateProvider(array $params);
}