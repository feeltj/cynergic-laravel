<?php

namespace App\Contracts;

interface UserRateDefaultsContract
{
   public function listUserRateDefaults(string $order ='id', string $sort = 'order', array $columns =['*']);
   public function findUserRateDefaultsById(int $id);
   public function updateUserRateDefaults(array $params);
}