<?php

namespace App\Contracts;

interface DomainContract 
{
     /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listDomains(string $order = 'id', $sort = 'desc', array $columns  = ['*']);
      /**
     * @param int $id
     * @return mixed
     */
   public function findDomainById(int $id);
    /**
     * @param array $params
     * @return mixed
     */


     /**
     * @param array $params
     * @return mixed
     */

    public function updateDomain(array $params);

     /**
     * @param $id
     * @return bool
     */
    public function deleteDomain($id);
}