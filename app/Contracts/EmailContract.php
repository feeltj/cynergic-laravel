<?php

namespace App\Contracts;

interface EmailContract 
{
     /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */
    public function listEmails(string $order = 'id', $sort = 'desc', array $columns  = ['*']);
      /**
     * @param int $id
     * @return mixed
     */
   public function findEmailById(int $id);
    /**
     * @param array $params
     * @return mixed
     */


     /**
     * @param array $params
     * @return mixed
     */

    public function updateEmail(array $params);

     /**
     * @param $id
     * @return bool
     */
    public function deleteEmail($id);
}