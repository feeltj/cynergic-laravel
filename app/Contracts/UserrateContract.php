<?php

namespace App\Contracts;

interface UserrateContract {
    public function listUserrate(string $order = 'id', string $sort = 'order', array $columns= ['*']);
    public function findUserRateById(int $id);
    public function updateUserRate(array $params);
}