<?php

namespace App\Contracts;

interface ContactContract {
    public function listContact(string $order = 'id', string $sort = 'desc', array $columns = ['*']);
    public function findContactById(int $id);
    public function updateContact(array $params);
    public function deleteContact($id);
}