<?php

namespace App\Contracts;

interface TicketContract 
{
    public function listTickets(string $order = 'id', $sort = 'desc', array $colums = ['*']);

    public function getTicketById($id);

    public function updateTcket(array $params);

    public function deleteTicket($id);
}