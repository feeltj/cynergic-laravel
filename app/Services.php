<?php
namespace App;
use Auth;
use DB;

class Services extends Dashboard  {
    
    public function getServices_domains($detail=false) {

	   

			$username=Auth::user()->username;

		$user_id=Auth::user()->id;
        $username=$this->getUsersParentChildren("username");
        //dd($users);
		$userIds=$this->getUsersParentChildren();
		$domains=array();
		$done=array();

		//foreach($users as $username) {

           // $userId=	$cynSql->sqlValue("user_info",array("username"=>$username),"id");
            
            $userId = DB::table('user_info')
                             ->where('username', $username)
                             ->orderBy('id')
                             ->get();
                             //dd($userId);

			/*$query="SELECT * FROM virtual_servers 
            WHERE deleted='no' && domain_user !='' 
            && domain_user='".mysql_real_escape_string($username)."' ORDER BY domain_name";
			//echo $query."<br>";
			$sql=$db->query($query);
            $doms=$sql->fetchAll();*/
            
            $doms = DB::table('virtual_servers')
                       ->where('deleted', 'no')
                       ->where('domain_user','!=', '')
                       ->where('domain_user', $username)
                       ->orderBy('domain_name')
                       ->get();
                       
			foreach($doms as $dom) {
                $domain=$dom->domain_name;
                //dd($domain);
                foreach($userId as $uid){
				if($dom->domain_billing_userid > 0) {
				    $uid=$dom->domain_billing_userid;
				}
				if(!in_array($domain,$done)) {

					//$domains[]=$dom;

					if($detail == true) {

                        $domainInfo=$this->getServices_domainData($domain);
                        //dd($domainInfo);
                      
						$dom->domainInfo=$domainInfo;
						$dom->object="user_info";
						$dom->object_id=$userId;
						$list[]=$dom;

					} else {
						$list[]=$domain;
					}

					$done[]=$domain;
					$this->usersDone[]=$userId;

				}
				//$list[]=$thing;

            }
        }
        //}
        //dd($list);

		return $list;


    }
    public function getUsersParentChildren($field="id") {
		
		
		$user_id=Auth::user()->id;
        
        $arr = DB::table('user_info')
                   ->where('parent_id', $user_id)
                   ->where('status', 'active')
                   ->orderBy('username')
                   ->get()
                   ;
                   //dd($arr);
		foreach($arr as $ui) {
		}
		return $ui->username;

    }
    public function getServices_domainData($domain)
    {
       
        $domain = DB::table('virtual_servers')
                  ->where('deleted', 'no')
                  ->where('domain_user','!=', '')
                  ->where('domain_name', $domain)
                  ->get();
                  //dd($domain);
        foreach($domain as $d){
        $id=$d->id;
        //dd($id);

        
        $list = DB::table('virtual_servers_data')
                   ->where('domain_id', $id)
                   ->get();
                   //dd($list);

		$nv=array();
		foreach($list as $inf) {
			if($inf->name <> "") {
				$nv[$inf->name]=$inf->value;
			}
        }
        //dd($nv);
		return $nv;
    }
}
public function getUserDomains() {

    $rows=array();
    //global $cynSql;
    $list=$this->getServices_domains(true);
    //dd($list);
    //$so = new serviceObjects();
    //$debug=print_r($list,true);

    $fields=array("domain_name","dns","expiry","cert");
    //dd($fields);
    if(is_array($list)) {
        if(count($list) > 0) {

            foreach($list as $item) {
                $serviceUrl='';
                //echo "object_id:".$item['object_id'];
                $userId=$item->object_id;
                //dd($userId);
               /*if($userId > 0) {

                    $user=$cynSql->sqlValue("user_info",array("id"=>$userId));
                    $rate_id=$user['rate_id'];
                    //$sql=$db->query("SELECT * FROM user_rates WHERE id='".mysql_real_escape_string($rate_id)."'");
                    //$rate=$sql->fetch();
                    $rate=$cynSql->sqlValue("user_rates",array("id"=>$rate_id));
                    $urd_id=$rate['default_id'];
                    if($title == "") {
                        $title=$rate['description'];
                    }
                    //$sql=$db->query("SELECT * FROM user_rates_defaults WHERE id='".mysql_real_escape_string($urd_id)."'");
                    //$urd=$sql->fetch();
                    $urd=$cynSql->sqlValue("user_rates_defaults",array("id"=>$urd_id));
                    $skip=0;
                    $group=trim(strtolower($urd['description']));

                    //echo "group:".$group;
                    if($group <> "domains") {} else {
                        $serviceUrl=$this->getServiceUrl($userId);
                    }

                }*/

                //print_r($item);

                $title=$item->domain_name;

                if($serviceUrl <> "") {
                    $title="<a href=\"".$serviceUrl."\">".$title."</a>";
                }
                $dnsIp=$item->dns_master_ip;
                //dd($dnsIp);

                $manageDNS=" ";
                if($dnsIp=="203.55.18.8") {
                    $manageDNS = " <a class=\"manage\" id=\"".$item->domain_name."\">Manage DNS</a>";
                }
                $item->dns=$manageDNS;
                

                if($item->expiry == "0000-00-00 00:00:00") {
                    $exp=$item->domainInfo;
                    //dd($exp);
                    if($exp <> "") {
                        $expd=explode("-",$exp);
                        $expd=array_reverse($expd);
                        $item->expiry=implode("-",$expd);
                    } else {
                        $item->expiry="n/a";
                    }
                    $exp=$item->domainInfo;
                } else {
                    $item->expiry=date("Y-m-d",strtotime($item->expiry));
                }

                $item->expiry=$item->domainInfo;

                if($item->domainInfo <> "") {
                    $item->cert=$this->buttons('domain_cert',$item->domain_name,array("title"=>"Download Certificate"));
                } else {
                    $item->cert=" ";
                }

                $item->domain_name=$title;


            }
        }
    }
    if(count($rows)==0) {
        //$rows[]="<td colspan=\"".count($fields)."\">No domains listed.</td>";
    }

    //$res= implode($item);
    //dd($item);
    //$res .= $debug;
    return $item;

}
public function domainInfo($DomainName, $FullContacts=0) {

    //$db=registrant_company

    $nameValues= $this->getServices_domainData($DomainName);
    //$company=$nameValues['registrant_company'];
    $company="";
    if($company == "") {
        dd($company);
        //https://ssl.twoplums.com.au/tppautomation/production/info/DomainInfo.php // Production URL
        $out= $this->tppDomainInfo($DomainName);
        $this->tppDomainInfo_parse($DomainName,$out);
        $nameValues= $this->getServices_domainData($DomainName);
    }

    $resp=$nameValues;
    return $resp;
}
public function buttons($which,$id,$args=array()) {

    $method="buttons_".$which;
    if(method_exists($this,$method)) {
        return $this->$method($which,$id);
    }

    if(is_array($args)) {extract($args);}
    if(!isset($title) OR $title=='') {
        $title=$which;
    }

    $out="<button name=\"launchWin.".$which."\" rel=\"".$id."\">".ucwords(str_replace("_"," ",$title))."</button>";
    return $out;

}
public function getServices_hosting() {

  


        $username=Auth::user()->username;

    $groupNames=array(
        "Hosting","Domains","Email"
    );

    $user_id=Auth::user()->id;
    $domains=$this->getServices_domains(true);

    $nodes=array();
    $usersDone=array();
    if(is_array($domains)) {
        if(count($domains) > 0) {
            $dUsers=array();
            foreach($domains as $d) {
                if(!in_array($d->domain_user,$dUsers)) {
                    $dUsers[]=$d->domain_user;
                }
            }
            foreach($dUsers as $username) {

            foreach($domains as $domain) {
                if($domain->domain_user==$username) {
                    $hostDomains[]=$domain->domain_name;
                }
                $domainString=implode("<br>",$hostDomains);
            }

                if($domain->domain_user==$username) {

                $domain_name=$domain->domain_name;
                $domain_user=$domain->domain_user;
                //dd($domain_user);

                $statsUrl=	$this->getWebStatsUrl($domain);

                if($this->_adminIP()==true) {
                    //echo "awstats:".$awstats."\n";
                }

                $statsLink=$domain_user;
                if(stripos($statsUrl,$domain->domain_name) > -1) {
                    //$statsLink="<a target=_blank href=\"".$statsUrl."\">".$domain_user."</a>";
                    $statsLink=$statsUrl;
                    //dd($statsLink);
                }


                $r=$this->findDomainRate($domain_name);
                $uinfo=$this->findDomainUser($domain_name);
                //dd($uinfo);

                $args['object']="user_info";
                $args['object_id']=$uinfo->id;

                //$serviceUrl=$this->getServiceUrl($uinfo['id']);
                //$serviceLink="<a href=\"".$serviceUrl."&s=hosting\">".$domain_user."</a>";
                //if($statsLink <> "") {
                 //   $serviceLink .= " [<a href=\"".$statsLink."\">stats</a>]";
               // }

                foreach($r as $rate){

                $title=$rate->description;
                //dd($title);
                //Dashboard $service = new Dashboard();
                //dd($uinfo);
                $table = $this->getUserRateUsageTable($uinfo);
                //dd($table);
                $usage = $this->getUsage($domain_name,$table,'','',true);
                //dd($usage);
                $email="";
               // $email=$this->getDomain_emails($domain_name);
                $email=count($email);
                $cols=array();
                $cols['userid']=$uinfo->id;
                $cols['username'] = $domain_user;
                $cols['domain']=$domainString;
                $cols['plan']=$title;
               // dd($cols);
                if($email == 0) {$email="n/a";}
                //$cols['email']=$email;
                $cols['dns']="<a target=_blank href=\"http://www.intodns.com/".$domain_name."\">DNS</a>";
                $cols['up']=ceil($usage[0]['up']);
                if($cols['up']==0) {$cols['up']="n/a";}
                dd($cols);
                //$cols['down']=ceil($usage[0]['down']);

                //if($domain['host_billing_userid'] > 0) {

                //}

                $url=$this->getCupsLink("usagegraph",$domain_name);
                $cols['graph']="<a class=\"btn i_graph\" href=\"".$url."\"></a>";

                if(in_array($domain['domain_user'],$usersDone)) {
                    //$cols['username']='';
                }

                $nodes[]=$this->formatTableCells($cols,$args);

                $usersDone[]=$domain['domain_user'];

                $this->usersDone[]=$uinfo['id'];
                }
            }

            }
        }
    }

    return implode($nodes);

}
public function getWebStatsUrl($domain) {

    //global $cynSql;

    $domain_name=$domain->domain_name;
   
    $query = DB::table('virtual_servers_servers')
            ->select('virtual_servers_servers_data.value')
            ->join('virtual_servers_servers_data', 'virtual_servers_servers_data.server_id', 'virtual_servers_servers.id')
            ->join('virtual_servers', 'virtual_servers.web_ip', 'virtual_servers_servers.ip')
            ->where('virtual_servers.domain_name', $domain_name)
            ->where('virtual_servers_servers_data.name', 'webstats_url')
            ->get();
     
   
    foreach($query as $aws){
        $tpl=	$aws->value;
        //dd($tpl);
        $contArr=	array("domain"=>$domain_name,"prot"=>"http");
        //dd($contArr);
        $out=	$this->renderTemplate($tpl,$contArr);
       
    
    }
    //dd($out);
    return $out;
  

}
public function renderTemplate($template,$content,$ns=array()) {

    foreach($content as $n => $v) {


        array_push($ns,$n);

        if(is_array($v)) {

            $template=$this->renderTemplate($template,$v,$ns);
        }
        $n=implode(".",$ns);
        $tag="{%".$n."%}";

        $template=str_replace($tag,$v,$template);

        array_pop($ns);
    }

    return $template;
}
public function _adminIP() {

    $adminIps=array("203.31.101.237","103.29.77.251","203.57.78.150","58.96.114.154");
    //$adminIps=array("203.31.101.237","58.96.114.154");
    if(in_array($_SERVER['REMOTE_ADDR'],$adminIps)) {
    return true;
    } else {
    return false;
    }

}

public function findDomainRate($domain) {

    $rate=false;


    $v = DB::table('virtual_servers')
                ->where('deleted', '!=', '')
                ->where('domain_name', $domain)
                ->get();
                //dd($vs);
    
    foreach($v as $vs){
        $user=$vs->domain_user;
        //dd($user);
        if($vs->host_billing_userid > 0) {
            $user_id=$vs->host_billing_userid;
        
            $sql = DB::table('user_info')
                      ->join('user_rate_infos', 'user_info.id', 'user_rate_infos.user_id')
                      ->join('user_rates', 'user_rates.id', 'user_rate_infos.rate_id')
                      ->where('user_info.id', Auth::user()->id)
                      ->where('user_info.id', $user_id)
                      ->where('status', 'active')
                      ->get();
                      foreach($sql as $u){
                        $rate_id=$u->rate_id;
                      }
                     //dd($rate_id);

              
        } else {
           
            $sql = DB::table('user_info')
            ->where('username', $user)
            ->where('status', 'active')
            ->get();
          foreach($sql as $u){
               $rate_id=$u->rate_id;
          }

   
        }
        if($rate_id > 0) {
            foreach($sql as $rate){
                //$sql=$db->query("SELECT * FROM user_rates WHERE id='".mysql_real_escape_string($rate_id)."'");
                $rate = DB::table('user_rates')
                        ->where('id', $rate_id)
                        ->get();
                       // dd($rate);
            }
           
    
        }
    
    return $rate;
    }
}
public function findDomainUser($domain) {

   
        $rate=false;
      

        $v = DB::table('virtual_servers')
               ->where('deleted', '!=', '')
               ->where('domain_name', $domain)
               ->get();
               //dd($vs);
    foreach($v as $vs){

  
        $user=$vs->domain_user;
        if($vs->host_billing_userid > 0) {
            $user_id=$vs->host_billing_userid;
            

            $user_in = DB::table('user_info')
                 ->where('id', $user_id)
                 ->where('status', 'active')
                 ->get();
        }
        foreach($user_in as $u){
            return $u;
        }
        
        }
     

       
}



}