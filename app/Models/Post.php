<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'address';

    public $timestamps = false;

    public function users() {
        return $this->hasMany('App\Models\User');
    }
}
