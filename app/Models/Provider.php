<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'user_rates_providers';
    public function rate() {
        return $this->belongsTo('App\Models\Rate');
    }
}
