<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'user_rates';
    public function users() {
        return $this->hasMany('App\Models\User');
    }

    public function default() {
        return $this->belongsTo('App\Models\Defaults');
    }

    public function provider() {
        return $this->hasMany('App\Models\Provider','userrate_id');
    }
}
