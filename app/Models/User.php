<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_info';
    protected $fillable = [
        'username', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $timestamps = false;

    public function address(){
        return $this->belongsTo('App\Models\Address');
    }
    public function contact(){
        return $this->belongsTo('App\Models\Contact');
    }
    public function rate() {
       return $this->belongsTo('App\Models\Rate');
    }
    public function findForPassport($username) {
        return self::where('username', $username)->first(); // change column name whatever you use in credentials
     }

    public function sendPasswordResetNotification($token)
    {
       $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }
}
