<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Defaults extends Model
{
    protected $table = 'user_rates_defaults';

    public function rates() {
        return $this->hasMany('App\Models\Rate', 'default_id');
    }
}
?>
