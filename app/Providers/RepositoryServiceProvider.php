<?php

namespace App\Providers;

use App\Contracts\AddressContract;
use App\Contracts\ContactContract;
use App\Contracts\UserinfoContract;
use App\Contracts\UserrateContract;
use App\Contracts\UserRateProvidersContract;
use App\Contracts\UserRateDefaultsContract;
use App\Contracts\DomainContract;
use App\Contracts\HostingContract;
use App\Contracts\EmailContract;
use App\Contracts\TicketContract;
use Illuminate\Support\ServiceProvider;
use App\Repositories\AddressRepository;
use App\Repositories\ContactRepository;
use App\Repositories\UserinfoRepository;
use App\Repositories\UserrateRepository;
use App\Repositories\UserRateProvidersRepository;
use App\Repositories\UserRateDefaultsRepository;
use App\Repositories\DomainRepository;
use App\Repositories\HostingRepository;
use App\Repositories\EmailRepository;
use App\Repositories\TicketRepository;



class RepositoryServiceProvider extends ServiceProvider
{
    protected $repositories = [
        AddressContract::class         =>          AddressRepository::class,
        ContactContract::class => ContactRepository::class,
        UserinfoContract::class => UserinfoRepository::class,
        UserrateContract::class => UserrateRepository::class,
        UserRateProvidersContract::class => UserRateProvidersRepository::class,
        UserRateDefaultsContract::class => UserRateDefaultsRepository::class,
        DomainContract::class => DomainRepository::class,
        HostingContract::class => HostingRepository::class,
        EmailContract::class => EmailRepository::class,
        TicketContract::class => TicketRepository::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $implementation)
        {
            $this->app->bind($interface, $implementation);
        }
    }
}
