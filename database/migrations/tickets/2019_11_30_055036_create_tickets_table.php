<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('ticketStatus');
            $table->string('summary');
            $table->string('contactName');
            $table->string('priority');
            $table->text('detailDescription');
            $table->unsignedBigInteger('user_id')->index('user_id');
            $table->unsignedBigInteger('department_id')->index('department_id');
            //$table->foreign('user_id')->references('id')->on('user_info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
