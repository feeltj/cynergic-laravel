import layout_routes from './layout'
import store from '../store/store'

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/login')
}

const routes = [{
    path: '/',
    component: resolve => require(['src/layout'], resolve),
    children: layout_routes
    }, {
        path: '/login',
        component: resolve => require(['pages/login'], resolve),
    
        meta: {
            title: "Login",
            beforeEnter: ifNotAuthenticated,
        }
    }, /*{
        path: '/register',
        component: resolve => require(['pages/register'], resolve),
        meta: {
            title: "register"
        }
    },*/ {
        path: '/password/reset',
        component: resolve => require(['pages/password'], resolve),
        meta: {
            title: "Forgot Password",
            beforeEnter: ifNotAuthenticated,
        }
    }, {
        path: '/password/reset/:token',
        component: resolve => require(['pages/reset'], resolve),
        meta: {
            title: "Reset Password",
            beforeEnter: ifNotAuthenticated,
        }
    }, /*{
        path: '/lockscreen',
        component: resolve => require(['pages/lockscreen'], resolve),
        meta: {
            title: "Lockscreen",
            guest: true,
        }
    },*/ {
        path: '/500',
        component: resolve => require(['pages/500'], resolve),
        meta: {
            title: "500"
        }
    },
    {
        path: '*',
        component: resolve => require(['pages/404'], resolve),
        meta: {
            title: "404"
        }
    }
]
export default routes
