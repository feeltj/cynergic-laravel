import store from '../store/store'

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/')
}
const layout = [{
    path: '/',
    component: resolve => require(['pages/index'], resolve),
 
    meta: {
        title: "Dashboard",
        requiresAuth: true,
        beforeEnter: ifAuthenticated,
    }
}, 
{
    
    path: '/services/:id',
    component: resolve => require(['pages/services'], resolve),
    meta: {
        title: "Services List",
        requiresAuth: true,
        beforeEnter: ifAuthenticated,
    }
},
{
    
    path: '/services',
    component: resolve => require(['pages/services'], resolve),
    meta: {
        title: "Services List",
        requiresAuth: true,
        beforeEnter: ifAuthenticated,
    }
},
{
    
    path: '/details/:id',
    component: resolve => require(['pages/details'], resolve),
    meta: {
        title: "Service",
        requiresAuth: true,
        props: true ,
        beforeEnter: ifAuthenticated,
    }
},
{
    path: 'tickets',
    component: resolve => require(['pages/tickets'], resolve),
    meta: {
        title: "Tickets",
        requiresAuth: true,
        beforeEnter: ifAuthenticated,
    }
},
{
    path: 'statement',
    component: resolve => require(['pages/statement'], resolve),
    meta: {
        title: "Statement",
        requiresAuth: true
    }
},
{
    path: 'payment',
    component: resolve => require(['pages/payment'], resolve),
    meta: {
        title: "Make a Payment",
        requiresAuth: true
    }
},
{
    path: 'servicerequest/provision',
    component: resolve => require(['pages/servicerequest'], resolve),
    meta: {
        title: "Service Request",
        requiresAuth: true
    }
}, {
    path: 'servicerequest/cancel',
    component: resolve => require(['pages/servicecancel'], resolve),
    meta: {
        title: "Service Request",
        requiresAuth: true
    }
}, 
{
    path: 'feedback',
    component: resolve => require(['pages/feedback'], resolve),
    meta: {
        title: "Contact US",
        requiresAuth: true
    }
},
{
    path: 'news',
    component: resolve => require(['pages/news'], resolve),
    meta: {
        title: "Whats New",
        requiresAuth: true
    }
},
{
    path: 'notifications',
        component: resolve => require(['pages/notifications'], resolve),
        meta: {
        title: "Notifications",
        requiresAuth: true
    }
},
{
    path: '/product_details',
    component: resolve => require(['pages/e-commerce/product_details'], resolve),
    meta: {
        title: "Product details",
    }
}, {
    path: '/product_edit',
    component: resolve => require(['pages/e-commerce/product_edit'], resolve),
    meta: {
        title: "Product Edit",
    }
}, {
    path: '/e_dashboard',
    component: resolve => require(['pages/e-commerce/e_dashboard'], resolve),
    meta: {
        title: "E dashboard",
    }
}, {
    path: '/cart_details',
    component: resolve => require(['pages/e-commerce/cart_details'], resolve),
    meta: {
        title: "Cart Details",
    }
}, {
    path: '/product_gallery',
    component: resolve => require(['pages/e-commerce/product_gallery'], resolve),
    meta: {
        title: "Product gallery",
    }
}, {
    path: 'form_elements',
    component: resolve => require(['pages/form_elements'], resolve),
    meta: {
        title: "Form Elements",
    }
}, {
    path: 'form_validations',
    component: resolve => require(['pages/form_validations'], resolve),
    meta: {
        title: " Form Validations",
    }
}, {
    path: 'dropdowns',
    component: resolve => require(['pages/dropdowns'], resolve),
    meta: {
        title: " Dropdowns",
    }
}, {
    path: 'cards',
    component: resolve => require(['pages/card'], resolve),
    meta: {
        title: " Cards",
    }
}, {
    path: 'buttons',
    component: resolve => require(['pages/buttons'], resolve),
    meta: {
        title: "Buttons",
    }
}, {
    path: 'radios_checkboxes',
    component: resolve => require(['pages/radios_checkboxes'], resolve),
    meta: {
        title: " Radios & Checkboxes",
    }
}, {
    path: 'vue-datepicker',
    component: resolve => require(['pages/vue-datepicker'], resolve),
    meta: {
        title: " Datepickers",
    }
}, {
    path: 'form_editors',
    component: resolve => require(['pages/form_editors'], resolve),
    meta: {
        title: " Form Editors",
    }
}, {
    path: 'notification',
    component: resolve => require(['pages/notifications2'], resolve),
    meta: {
        title: " Notification",
    }
}, {
    path: 'modals',
    component: resolve => require(['pages/modals'], resolve),
    meta: {
        title: " Modals",
    }
}, {
    path: 'vscroll',
    component: resolve => require(['pages/vscroll'], resolve),
    meta: {
        title: " Vscroll",
    }
}, {
    path: 'vue-slider',
    component: resolve => require(['pages/vue_slider'], resolve),
    meta: {
        title: " Vue Slider",
    }
}, {
    path: 'ui_elements',
    component: resolve => require(['pages/ui_elements'], resolve),
    meta: {
        title: " UI Elements",
    }
},  {
    path: 'api',
    component: resolve => require(['pages/api'], resolve),
    meta: {
        title: "API",
    }
}, {
    path: 'timeline',
    component: resolve => require(['pages/timeline'], resolve),
    meta: {
        title: "Timeline",
    }
}, {
    path: 'chat',
    component: resolve => require(['pages/chat'], resolve),
    meta: {
        title: "Chat",
    }
},  {
    path: 'simple_tables',
    component: resolve => require(['pages/simple_tables'], resolve),
    meta: {
        title: "Simple Tables",
    }
}, {
    path: 'advanced_tables',
    component: resolve => require(['pages/advanced_tables'], resolve),
    meta: {
        title: "Advanced Tables",
    }
}, {
    path: 'chartist',
    component: resolve => require(['pages/chartist'], resolve),
    meta: {
        title: "Chartist Charts",
    }
}, {
    path: 'frappe-charts',
    component: resolve => require(['pages/frappe_charts'], resolve),
    meta: {
        title: "Frappe Charts",
    }
}, {
    path: 'e_linecharts',
    component: resolve => require(['pages/e_linecharts'], resolve),
    meta: {
        title: "Echarts - Line",
    }
}, {
    path: 'e_barcharts',
    component: resolve => require(['pages/e_barcharts'], resolve),
    meta: {
        title: "Echarts - Bar",
    }
}, {
    path: 'e_piecharts',
    component: resolve => require(['pages/e_piecharts'], resolve),
    meta: {
        title: "Echarts - Pie",
    }
}, {
    path: 'trend_bar',
    component: resolve => require(['pages/trend_bar'], resolve),
    meta: {
        title: "Vue Trend/Bar charts",
    }
},  {
    path: 'gallery',
    component: resolve => require(['pages/gallery'], resolve),
    meta: {
        title: "Gallery",
    }
}, {
    path: 'user_profile',
    component: resolve => require(['pages/user_profile'], resolve),
    meta: {
        title: "User Profile",
    }
}, {
    path: 'add_user',
    component: resolve => require(['pages/add_user'], resolve),
    meta: {
        title: "Add User",
    }
}, {
    path: 'users_list',
    component: resolve => require(['pages/users_list'], resolve),
    meta: {
        title: "Users List",
    }
}, {
    path: 'edit_user',
    component: resolve => require(['pages/edit_user'], resolve),
    meta: {
        title: "Edit User",
    }
}, {
    path: 'blank',
    component: resolve => require(['pages/blank'], resolve),
    meta: {
        title: "Blank",
    }
}, {
    path: 'transitions',
    component: resolve => require(['pages/transitions'], resolve),
    meta: {
        title: "Transitions",
    }
}, {
    path: 'invoice',
    component: resolve => require(['pages/invoice'], resolve),
    meta: {
        title: "Invoice",
    }
}, {
    path: 'contact_us',
    component: resolve => require(['pages/contact_us'], resolve),
    meta: {
        title: "Contact Us",
    }
}, {
    path: 'contacts',
        component: resolve => require(['pages/contacts'], resolve),
        meta: {
        title: "contacts",
    }
}]

export default layout
