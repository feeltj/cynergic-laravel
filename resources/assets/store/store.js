import 'es6-promise/auto'
import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import { getLocalUser } from '../helpers/auth';

import user from '../vuex/modules/user'
import auth from '../vuex/modules/auth'

Vue.use(Vuex)

function addDays(noOfDays) {
    return (noOfDays * 24 * 60 * 60 * 1000)
}
//const user = getLocalUser();
//=======vuex store start===========
const store = new Vuex.Store({
    state: {
        left_open: true,
        preloader: true,
        site_name: "Сynergic",
        page_title: null,
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        customers: [],
        user: {
            name: "Alex",
            picture: require("img/authors/prf4.jpg"),
            job: "Web Developer"
        },
        cal_events: [{
            id: 0,
            title: 'Office',
            start: Date.now(),
            end: Date.now() + addDays(1)
        }, {
            id: 1,
            title: 'Holidays',
            start: Date.now() + addDays(3),
            end: Date.now() + addDays(4)
        }],
        // Add your application keys
        gmap_key: 'AIzaSyBTnQCx3FXEnfWPPWTKAwIxt6wSjAn_8ug',
        openWeather_key: 'c00194f61244d2b33b863bff6d94e663',
        google_analytics_key: null

    },
    getters: {
           isLoading: state=> state.isLoading,
           isLoggedIn: state =>state.isLoggedIn,
           currentUser: state => state.currentUser,
           
           authError: state => state.auth_error
    },
    mutations,
    actions: {
        login(context) {
            context.commit("login")
        }
    },
    modules: {
        user,
        auth,
    }
})
//=======vuex store end===========
export default store
