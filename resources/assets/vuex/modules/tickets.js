import Vue from "vue";



    const state = {
        tickets: [],
    },

    const getters = {
    
        getTicketById: state => id => {
            return state.tickets.find(ticket => ticket.id === id)
        },
    },

    const actions =  {

        fetchTickets({ commit }) {
            return axios.get('api/setTickets')
                .then(response => {
                    commit('setTickets', response.data.tickets);
                });
        },
    },

    const mutations = {

        setTickets(state, tickets) {
            state.tickets = tickets;
        },
    }
    export default {
        state,
        getters,
        actions,
        mutations,
    }
