const menu_items = [{
        name: 'Dashboard',
        link: '/',
        icon: ' fa fa-home'
    },{
        name: 'Services',
        link: '/services',
        icon: 'fa fa-desktop'
    },
    {
        name: 'Tickets',
        link: '/tickets',
        icon: 'fa fa-ticket'
    },
    {
        name: 'Billing',
        link: '/statement',
        icon: 'fa fa-bank'
    },
    {
        name: 'Make a payment',
        link: '/payment',
        icon: 'fa fa-money'
    },
    {
        name: "Service Requests",
        icon: "fa fa-users",
        child: [{
            name: 'New services',
            link: '/servicerequest/provision',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Cancel services',
            link: '/servicerequest/cancel',
            icon: 'fa fa-angle-double-right'
        }]
    },
    {
        name: 'Contact US',
        link: '/feedback',
        icon: 'fa fa-comments-o'
    },
    {
        name: 'Whats new', // <span class="badge-success badge pull-right">{{this.$store.state.cal_events.length}}</span>
        link: '/news',
        icon: 'fa fa-newspaper-o'
    },
    {
        name: 'Notifications',
        link: '/notifications',
        icon: 'fa fa-bell'
    },
    
    /*{
    name: 'Contacts',
    link: '/contacts',
    icon: ' fa fa-address-book-o'
    },
  
     {
        name: 'E-Commerce',
        icon: 'fa fa-shopping-cart',
        child: [{
            name: 'E Dashboard',
            link: '/e_dashboard',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Cart Details',
            link: '/cart_details',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Product details',
            link: '/product_details',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Product Edit ',
            link: '/product_edit',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Product gallery ',
            link: '/product_gallery',
            icon: 'fa fa-angle-double-right'
        }]
    }, {
        name: 'Forms',
        icon: 'fa fa-pencil-square-o',
        child: [{
            name: 'Form elements',
            link: '/form_elements',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Form validations',
            link: '/form_validations',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Form editors',
            link: '/form_editors',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Dropdowns',
            link: '/dropdowns',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Radios & Checkboxes',
            link: '/radios_checkboxes',
            icon: 'fa fa-angle-double-right'
        }]
    },  {
        name: 'API',
        link: '/api',
        icon: 'fa fa-clone'
    },
    {
        name: 'UI Components',
        title: ""
    }, {
        name: 'Components',
        icon: 'fa fa-globe',
        child: [{
            name: 'UI elements',
            link: '/ui_elements',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Cards',
            link: '/cards',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Buttons',
            link: '/buttons',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Vscroll',
            link: '/vscroll',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Chat',
            link: '/chat',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Modals',
            link: '/modals',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Vue-Datepicker',
            link: '/vue-datepicker',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Vue slider',
            link: '/vue-slider',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Notifications',
            link: '/notification',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Timeline',
            link: '/timeline',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Transitions',
            link: '/transitions',
            icon: 'fa fa-angle-double-right'
        }]
    }, {
        name: 'Charts',
        title: ""
    }, {
        name: ' Charts',
        icon: 'fa fa-bar-chart',
        child: [{
            name: 'Chartist charts',
            link: '/chartist',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Frappe Charts',
            link: '/frappe-charts',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Echarts - Line',
            link: '/e_linecharts',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Echarts - Bar',
            link: '/e_barcharts',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Echarts - Pie',
            link: '/e_piecharts',
            icon: 'fa fa-angle-double-right'
        },{
            name: 'Vue Trend/Bar',
            link: '/trend_bar',
            icon: 'fa fa-angle-double-right'
        }]
    }, {
        name: 'Tables',
        icon: 'fa fa-table',
        child: [{
            name: 'Simple tables',
            link: '/simple_tables',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Advanced tables',
            link: '/advanced_tables',
            icon: 'fa fa-angle-double-right'
        }]
    }, {
        name: 'Files & Gallery',
        title: ""
    },  {
        name: "Users",
        icon: "fa fa-users",
        child: [{
            name: 'User profile',
            link: '/user_profile',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Add new user',
            link: '/add_user',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Users list',
            link: '/users_list',
            icon: 'fa fa-angle-double-right'
        }]
    }, {
        name: 'Pages',
        title: ""
    }, {
        name: "Pages",
        icon: "fa fa-files-o",
        child: [{
            name: 'Login',
            link: '/login',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Register',
            link: '/register',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Forgot password',
            link: '/forgotpassword',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Reset password',
            link: '/reset_password',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Lockscreen',
            link: '/lockscreen',
            icon: 'fa fa-angle-double-right'
        }]
    }, {
        name: "Extra Pages",
        icon: "fa fa-files-o",
        child: [{
            name: 'Blank',
            link: '/blank',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Invoice',
            link: '/invoice',
            icon: 'fa fa-angle-double-right'
        }, {
            name: 'Contact us',
            link: '/contact_us',
            icon: 'fa fa-angle-double-right'
        },  {
            name: '404',
            link: '/404',
            icon: 'fa fa-angle-double-right'
        }, {
            name: '500',
            link: '/500',
            icon: 'fa fa-angle-double-right'
        }]
    }*/
];
export default menu_items;
