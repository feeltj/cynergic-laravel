<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});
Route::get('details','Auth\UserController@details')->middleware('auth:api');
Route::post('login', 'Auth\UserController@login')->name('login');
Route::post('/logout', 'Auth\UserController@logout')->name('logout')->middleware('auth:api');
Route::get('/accountSummary', 'Dashboard\DashboardController@index')->middleware('auth:api');
Route::get('/getservice', 'Dashboard\DashboardController@getActiveServices')->middleware('auth:api');
Route::get('/getuserdata', 'Dashboard\DashboardController@getuserdata')->middleware('auth:api');
Route::post('/updateuser', 'Dashboard\DashboardController@update')->middleware('auth:api');
Route::post('/updateuserdata', 'Dashboard\DashboardController@updateUserData')->middleware('auth:api');
Route::get('/getservicemultiple', 'Dashboard\DashboardController@index')->middleware('auth:api');
Route::get('/services', 'Services\ServicesController@index')->middleware('auth:api');
Route::get('/headservice', 'Services\ServicesController@headService')->middleware('auth:api');
Route::get('/gethosting', 'Services\ServicesController@getHosting')->middleware('auth:api');
Route::get('/getdomains', 'Services\ServicesController@getDomains')->middleware('auth:api');
Route::post('/updatedomain', 'Services\ServicesController@updateDomain')->middleware('auth:api');
Route::get('/getemail', 'Services\ServicesController@getEmails')->middleware('auth:api');
Route::get('/getticket', 'Tickets\TicketController@index')->middleware('auth:api');
Route::get('/ticketcreate', 'Tickets\TicketController@create')->middleware('auth:api');
Route::post('/ticketcreate', 'Tickets\TicketController@store')->middleware('auth:api');


